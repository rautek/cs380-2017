#ifndef GLUTILS_H
#define GLUTILS_H


// include GLEW: http://glew.sourceforge.net/
#define GLEW_STATIC // including glew as static library
#include <GL/glew.h> 

// include glfw library: http://www.glfw.org/
#define GLFW_INCLUDE_GLCOREARB // defines that opengl core profile is used 
#include <GLFW/glfw3.h>

namespace GLUtils
{
    int checkForOpenGLError(const char *, int);
    
    void dumpGLInfo(bool dumpExtensions = false);
								
    void debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam);
}

#endif // GLUTILS_H
