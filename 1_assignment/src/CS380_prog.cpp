// CS 380 - GPGPU Programming, KAUST
//
// Programming Assignment #1

// includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <sstream>
#include <assert.h>

#include <math.h>

#include "GL/glew.h" 
#include "GL/freeglut.h"

// includes, cuda
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>


// window size
const unsigned int m_uWindowWidth = 512;
const unsigned int m_uWindowHeight = 512;

// run main loop 
boolean m_bRun = true;


// render a frame
void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// render models here

	glutSwapBuffers();
}



// keyboard event handler
void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
	switch (key)
	{
	case (27): // esc
		glutDestroyWindow(glutGetWindow());
		return;
	}
}


// query GPU functionality we need for OpenGL, return false when not available
bool queryGPUCapabilitiesOpenGL()
{
	// =============================================================================
	//TODO:
	// for all the following:
	// read up on concepts that you do not know and that are needed here!
	//
	// query and print (to console) OpenGL version and extensions:
	// - query and print GL vendor, renderer, and version using glGetString()
	//
	// find out how to query extensions with GLEW:
	//   http://glew.sourceforge.net/, http://www.opengl.org/registry/
	// - query and print whether your GPU supports:
	//   - 3D textures
	//   - texture arrays
	//   - support for frame buffer objects
	//   - support for the GLSL Shading Language, and which version of GLSL is supported
	//   - GLSL geometry shaders. see http://www.opengl.org/registry/specs/ARB/geometry_shader4.txt
	//     if you cannot use this extension look for GL_EXT_geometry_shader4 instead and update your graphics driver!
	// 
	// query and print GPU OpenGL limits (using glGet(), glGetInteger() etc.):
	// - maximum number of vertex shader attributes
	// - maximum number of varying floats
	// - number of texture image units (in vertex shader and in fragment shader, respectively)
	// - maximum 2D texture size
	// - maximum 3D texture size
	// - maximum number of draw buffers
	// =============================================================================

	return true;
}

// query GPU functionality we need for CUDA, return false when not available
bool queryGPUCapabilitiesCUDA()
{
	// Device Count
	int devCount;

	// Get the Device Count
	cudaGetDeviceCount(&devCount);
	
	// Print Device Count
	printf("Device(s): %i\n", devCount);
	
	// =============================================================================
	//TODO:
	// for all the following:
	// read up on concepts that you do not know and that are needed here!
	// 
	// query and print CUDA functionality:
	// - number of CUDA-capable GPUs in your system using cudaGetDeviceCount()
	// - CUDA device properties for every found GPU using cudaGetDeviceProperties():
	//   - device name
	//   - compute capability
	//   - multi-processor count
	//   - clock rate
	//   - total global memory
	//   - shared memory per block
	//   - num registers per block
	//   - warp size (in threads)
	//   - max threads per block
	// =============================================================================

	return true;
}


// check for opengl error and report if any
void checkForOpenGLError()
{
	GLenum error = glGetError();
	if (error==GL_NO_ERROR) {
		return;
	}
	std::string errorString = "unknown";
	std::string errorDescription = "undefined";

	if (error==GL_INVALID_ENUM)
	{
		errorString = "GL_INVALID_ENUM";
		errorDescription = "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_VALUE)
	{
		errorString = "GL_INVALID_VALUE";
		errorDescription = "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_OPERATION)
	{
		errorString = "GL_INVALID_OPERATION";
		errorDescription = "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_FRAMEBUFFER_OPERATION)
	{
		errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
		errorDescription = "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_OUT_OF_MEMORY)
	{
		errorString = "GL_OUT_OF_MEMORY";
		errorDescription = "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
	}
	else if (error==GL_STACK_UNDERFLOW)
	{
		errorString = "GL_STACK_UNDERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to underflow.";
	}
	else if (error==GL_STACK_OVERFLOW)
	{
		errorString = "GL_STACK_OVERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to overflow.";
	}

	fprintf(stdout, "Error %s: %s\n", errorString.c_str(), errorDescription.c_str());
}


// init opengl context and glew
bool initGL(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(m_uWindowWidth, m_uWindowHeight);
	glutCreateWindow("CS380 - OpenGL & CUDA Framework");

	glutDisplayFunc(render);
	glutKeyboardFunc(keyboard);
	
	// initialize necessary OpenGL extensions
	if (glewInit() != GLEW_OK)
	{
		std::cerr << "glewInit() failed!" << std::endl;
		return 0;
	}
	else 
	{
		std::string version((const char *)glGetString(GL_VERSION));
		std::stringstream stream(version);
		unsigned major, minor;
		char dot;

		stream >> major >> dot >> minor;

		assert(dot == '.');
		if (major > 3 || (major == 2 && minor >= 0)) {
			std::cout << "OpenGL Version " << major << "." << minor << std::endl;
		} else {
			std::cout << "The minimum required OpenGL version is not supported on this machine. Supported is only " << major << "." << minor << std::endl;
		}


	}


	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	// viewport
	glViewport(0, 0, m_uWindowWidth, m_uWindowHeight);

	// projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat)m_uWindowWidth / (GLfloat)m_uWindowHeight, 0.1, 10.0);		
	

	// init glew 
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		// glewInit failed, something is seriously wrong
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		return false;
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	return true;
}
 


// entry point
int main(int argc, char** argv)
{
	// init opengl
	if (!initGL(argc, argv)) 
	{
		// quit in case initialization fails
		return -1;
	}

	// query opengl capabilities
	if (!queryGPUCapabilitiesOpenGL()) 
	{
		// quit in case capabilities are insufficient
		return -1;
	}

	// query cuda capabilities
	if(!queryGPUCapabilitiesCUDA())
	{
		// quit in case capabilities are insufficient
		return -1;
	}

	// start traversing the main loop
	glutMainLoop();
}


