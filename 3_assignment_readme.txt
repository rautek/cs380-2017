=====================================================================
CS380 GPU and GPGPU Programming, KAUST
Programming Assignment #3
Image Processing with GLSL and CUDA

Contacts: 
peter.rautek@kaust.edu.sa
christopher.moore@kaust.edu.sa
=====================================================================

Tasks:

1. texture mapping
- read a sample texture from the folder "data/", use the bmpreader or any library you like. 
  you can also convert the texture to a different format before you read it. 
- render the box with different textures assigned to it

2. image processing with GLSL

- implement simple operations to adjust brightness, contrast, and saturation of the texture, as described in chapter 19.5 in the GLSL book
- implement smoothing, edge detection, and sharpening as described in chapter 19.7 in the GLSL book.
- basically there are two options how to implement this in OpenGL:
2.a) Frame buffer object (FBO) with two rendering passes
	pass 1: render the processed image into a target texture attached to an OpenGL FBO. 
	the target texture is different from the source texture!
	perform the image processing operation(s) in this rendering pass using normalized texture coordinate arithmetics.
	pass 2: use the resulting texture (from pass 1) and apply it as texture when rendering a mesh
2.b) Compute shader: Use a compoute shader to process the texture and use the resulting texture when rendering a mesh

Use method 2.a) in this assignment!
BONUS: also implement method 2.b)

3. image processing with CUDA

- implement the same image processing operations (as in point 2: brightness, contrast, saturation, smoothing, edge detection, sharpening), but this time using CUDA

Again there are two variantions, how to implement this:
3.a) Simple c-style array as input for CUDA:
   A simple c-style array is used as input to the CUDA kernels. 
   cudaMalloc is used to allocate memory and cudaMemcpy is used to initialize device memory. 
   Do not confuse this with the cudaArray type.

3.b) Texture as input for CUDA: 
   A texture is used as input for your CUDA kernels.
   This method uses the cudaArray type.
   
Use method 3.a) in this assignment!
BONUS: also implement method 3.b)

4. Submit your program and a report including results for the different image processing operations comparing the GLSL and the CUDA version.

All settings must be accessible from your program without the need to modify the source code. 
Use the following key mappings to switch between the image processing operations and GLSL vs. CUDA:
C: CUDA
G: OpenGL
B: Brightness
C: Contrast
S: Saturation
M: Smoothing
E: Edge Detection
H: Sharpening
Use additional key mappings as you see fit (+, -, 1, 2, 3, ...) and document them in the report.

The provided textures are from http://www.grsites.com/
You may add other textures as well